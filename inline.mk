# Libs and permissions
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/gapps/lib,system/lib)
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/gapps/lib64,system/lib64)
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/gapps/usr,system/usr)
PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/gapps/etc,system/etc)

# Framework
PRODUCT_PACKAGES += \
    framework/com.google.android.dialer.support \
    framework/com.google.android.maps \
    framework/com.google.android.media.effects

# /system/app/
PRODUCT_PACKAGES += \
    CalendarGooglePrebuilt \
    FaceLock \
    GoogleContactsSyncAdapter \
    GoogleExtShared \
    GoogleTTS

# /system/app/
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    AndroidPlatformServices \
    ConfigUpdater \
    ConnMetrics \
    GmsCoreSetupPrebuilt \
    GoogleBackupTransport \
    GoogleExtServices \
    GoogleFeedback \
    GoogleLoginService \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    MatchmakerPrebuilt \
    Phonesky \
    PrebuiltGmsCorePi \
    DynamiteModulesA \
    AdsDynamite \
    DynamiteModulesC \
    CronetDynamite \
    DynamiteLoader \
    GoogleCertificates \
    MapsDynamite \
    SetupWizard \
    Turbo \
    Velvet \
    WellbeingPrebuilt
